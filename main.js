document.getElementById('userForm').addEventListener('submit', function (event) {
  event.preventDefault(); // Prevent form submission

  // Get form values
  var name = document.getElementById('name').value;
  var email = document.getElementById('email').value;
  var password = document.getElementById('password').value;
  var confirmPassword = document.getElementById('confirmPassword').value;

  // Perform form validation
  if (name === '' || email === '' || password === '' || confirmPassword === '') {
    alert('Please fill out all fields');
    return;
  }

  if (password !== confirmPassword) {
    alert('Passwords do not match');
    return;
  }

  // Submit form data to server-side script
  // Replace the URL below with your server-side script URL
  var url = 'https://example.com/submit-user-form';
  var formData = {
    name: name,
    email: email,
    password: password,
  };

  // Send form data using fetch API
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formData),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Error: ' + response.status);
      }
    })
    .then(function (data) {
      // Display success message
      alert('User created successfully');
      // Reset form
      document.getElementById('userForm').reset();
    })
    .catch(function (error) {
      // Display error message
      alert('An error occurred: ' + error.message);
    });
});
